package org.levelup.job.list.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class JdbcUtils {

    // DriverManager
    // java.sql.Driver
//    static {
//        try {
//            Class.forName("org.postgresql.Driver");
//        } catch (ClassNotFoundException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public static Connection getConnection() {
        // url
        // jdbc:<vendor name>://<host(IP address)>:<port>/<db name>
        try {
            Connection connection = DriverManager.getConnection(
                    // localhost = 127.0.0.1
                    "jdbc:postgresql://localhost:5432/job_list",
                    "postgres",
                    "кщще"
            );
            return connection;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
